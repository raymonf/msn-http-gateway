const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const net = require('net');
const request = require('request-promise');

let rawBodySaver = function (req, res, buf, encoding) {
  if (buf && buf.length) {
    req.rawBody = buf.toString(encoding || 'utf8');
  }
}

app.use(bodyParser.raw({
	limit: '1024kb',
	inflate: true,
	verify: rawBodySaver,
	type: function () { return true },
}));

let clients = [];

app.all('/gateway/gateway.dll', function (req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'POST, GET');
	res.setHeader('Access-Control-Expose-Headers', 'X-MSN-Messenger');

	// req.query
	if(req.query.Action && req.query.Action.toLowerCase() == 'open')
	{
		if(req.query.Server && req.query.IP)
		{
			// to-do: validate Action, Server, and IP

			// better way?
			let sessionId = Math.floor(Math.random() * (9 * 100000000)).toString() + '.' + Math.floor(Math.random() * (9 * 10000)).toString();

			res.setHeader('Content-Type', 'application/x-msn-messenger');
			res.setHeader('X-MSN-Messenger', 'SessionID=' + sessionId + '; GW-IP=127.0.0.1');

			let sock = new net.Socket();

			clients.push({
				sessionId: sessionId,
				tcpSocket: sock,
				currentResponse: ''
			});

			try {
				sock.connect(1863, req.query.IP, function () {
					sock.write(req.rawBody);
					res.send('GWMSG: Your session ID is: ' + sessionId);
				});
			}
			catch(err) {
				let iClient = clients.find(function(client) {
					return client.tcpSocket == sock;
				});

				clients.splice(clients.indexOf(iClient), 1);
				res.send('GWERR: Connection error');
				return;
			}

			sock.on('data', function (data) {
				let client = clients.indexOf(clients.find(function(client) {
					return client.tcpSocket == sock;
				}));

				clients[client].currentResponse += data;
			});

			sock.on('close', function () {
				// idk
				let iClient = clients.find(function(client) {
					return client.tcpSocket == sock;
				});

				clients.splice(clients.indexOf(iClient), 1);
			});

			sock.on('error', function(e) {
				let iClient = clients.find(function(client) {
					return client.tcpSocket == sock;
				});

				clients.splice(clients.indexOf(iClient), 1);

				res.send('GWERR: Connection error');
			});
		} else {
			res.send('GWERR: Server and IP required')
		}
	} else if(req.query.SessionID) {
		let sessionId = req.query.SessionID;

		let iClient = clients.find(function(client) {
			return client.sessionId == sessionId;
		}.bind(sessionId));

		if(iClient == null) {
			res.send('GWERR: Invalid session ID');
			return;
		}

		res.setHeader('Content-Type', 'application/x-msn-messenger');
		res.setHeader('X-MSN-Messenger', 'SessionID=' + sessionId + '; GW-IP=127.0.0.1');

		// session id found
		if(req.query.Action)
		{
			// polling, send back current command list!
			res.send(iClient.currentResponse);

			// clear
			iClient.currentResponse = '';
		} else {
			// client wants to send shit to the server
			iClient.tcpSocket.write(req.rawBody);
			res.send('GWMSG: Sent');
		}
	} else {
		res.send('GWERR: Nothing to do');
	}
});

app.all('/nexus', function (req, res) {
	if(req.query.email && req.query.password && req.query.url)
	{
		var options = {
			url: req.query.url,
			headers: {
				'Authorization': 'Passport1.4 OrgVerb=GET,OrgURL=http%3A%2F%2Fmessenger%2Emsn%2Ecom,sign-in=' + encodeURIComponent(req.query.email) + ',pwd=' + encodeURIComponent(req.query.password) + ',lc=1033,id=507,tw=40,fs=1,ru=http%3A%2F%2Fmessenger%2Emsn%2Ecom,ct=1062764229,kpp=1,kv=5,ver=2.1.0173.1,tpf=43f8a4c8ed940c04e3740be46c4d1619'
			},
			resolveWithFullResponse: true
		};

		request(options)  
			.then(function (response) {
				if(response.headers['authentication-info']) {
					let authToken = response.headers['authentication-info'].split(/'/)[1];
					res.send(authToken);
				} else {
					res.send('GWERR: Unknown response');
				}
			})
			.catch(function (err) {
				if(err.statusCode == 401) {
					res.send('GWERR: Auth failure');
				} else {
					res.send('GWERR: Request error');
					console.log(err);
				}
			})
	} else {
		res.send('GWERR: <code>email</code>, <code>password</code>, and <code>url</code> required')
	}
});

app.listen(80, function () {
	console.log('Started MSN gateway');
});